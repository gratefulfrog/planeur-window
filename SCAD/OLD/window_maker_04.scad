
////////////////////
///// targets //////
////////////////////


//pivotBlock();
//latch();
//translate([0,0,10,])
//trap();

//topRail();
//bottomRail();

//topSpring();
//bottomSpring();

//plateAll();
//button();

//plateTest();
//holeTest();
//multiHoleTest();

////////////////////
///// end of targets //////
////////////////////

$fn = 100;

//dxfFileName = "../DXF/plate_05.dxf";
dxfFileName = "../DXF/plate_06_ecope_sim.dxf";

insertDia = 4.2;

outerContourLayerName     = "outer contour";
innerContourLayerName     = "inner contour";
trapOuterContourLayerName = "trap outer contour";
trapInnerContourLayerName = "trap inner contour";
handleLayerName           = "handle";
holesLayerName            = "holes";
trapInnerLayerName        = "trap inner";
trapOuterLayerName        = "trap outer";

fullZ = 4.6;
outerLipZ = fullZ-3; // = 1.6; // 1.5;
traplipZ = 3.3; // 3.0;
coneZ = 2;
coneOuterR = 6/2.0;
coneInnerR = 3/2.0;
buttonCoverZ = 1.6;

module outerLip(){
  linear_extrude(outerLipZ)
    import(dxfFileName, layer = outerContourLayerName);
}
//outerLip();

module innerZ(){
  linear_extrude(fullZ)
    import(dxfFileName, layer = innerContourLayerName);
}
//innerZ();

module handleHole(){
  linear_extrude(fullZ-buttonCoverZ)
    import(dxfFileName, layer = handleLayerName);
}
//handleHole();

module trapNegative(){
  epsilon = 0; // 0.5;
  linear_extrude(traplipZ)
    import(dxfFileName, layer = trapInnerContourLayerName);
  translate([0,0,traplipZ-epsilon])
    linear_extrude(fullZ-traplipZ +epsilon)
      import(dxfFileName, layer = trapOuterContourLayerName);
}
//trapNegative();

module holes(){
  linear_extrude(fullZ)
    import(dxfFileName, layer = holesLayerName);
}
//holes();

module cones(){
  translate([10,5.75,/*80.45,44.5,*/fullZ-coneZ])
    cylinder(coneZ,coneInnerR,coneOuterR);
  translate([10,33.75,/*80.45,72.5,*/fullZ-coneZ])
    cylinder(coneZ,coneInnerR,coneOuterR);
}
//cones();

module plateAll(){
  difference(){
    union(){
      outerLip();
      innerZ();
    }
    union(){
      handleHole();
      trapNegative();
      holes();
      cones();
    }
  }
}
//translate([0,4.6,0])
/*
rotate([180,0,0])
projection(cut=true)
translate([0,0,-43.5])
rotate([90,0,0])
translate([-70.45,-1,0])
plateAll();
*/

pivotZFix    = 1.6;
pivotBlockY  = 39.5;
pivotBlockX0 = 19;
pivotBlockX1 = 16;
pivotBlockeZ = 9+pivotZFix;


pivotBlockPointVec = [[0,0,0],
                      [pivotBlockX0,0,0],
                      [pivotBlockX0,pivotBlockY,0],
                      [0,pivotBlockY,0],
                      [0,0,pivotBlockeZ],
                      [pivotBlockX1,0,pivotBlockeZ],
                      [pivotBlockX1,pivotBlockY,pivotBlockeZ],
                      [0,pivotBlockY,pivotBlockeZ]];
pivotBlockFaceVec = [[0,1,2,3],  // bottom
  [4,5,1,0],  // front
  [7,6,5,4],  // top
  [5,6,2,1],  // right
  [6,7,3,2],  // back
  [7,4,0,3]];

module pivotBlock(){
  difference(){
    polyhedron(pivotBlockPointVec,pivotBlockFaceVec);
    union(){
      pivotHole();
      blockMountHoles();
    }
  }
}
//projection(cut=true)
//  translate([0,0,-1]) 
//projection(cut=true)
//rotate([90,0,0])
//pivotBlock();

pivotHoleX = 4.5;
pivotHoleZ = 4.75+pivotZFix;
pivotHoleR = 1.6 ; // + 0.1;

module pivotHole(){
  //scale([1,1.5,1])
  translate([pivotHoleX,0,pivotHoleZ])
    rotate([-90,0,0])
      cylinder(pivotBlockY,r=pivotHoleR);
}
//pivotHole();

mountHoleX      = 10;
mountHoleDeltaY = 28;
mountHoleY0     = (pivotBlockY - mountHoleDeltaY)/2.0;
mountHoleY1     = pivotBlockY - mountHoleY0;
mountHoleR      = insertDia/2.0;
mountHoleZ      = 5 + 1.5;

module blockMountHoles(){
  translate([mountHoleX,mountHoleY0,0])
    cylinder(mountHoleZ, r=mountHoleR);
  translate([mountHoleX,mountHoleY1,0])
    cylinder(mountHoleZ, r=mountHoleR);
}
//blockMountHoles();

latchSideX = 3;
latchInnerY = 40;
latchPlateX  = 24;
latchSideLayerName = "latchSide";
latchSupportLayerName = "latchSupport";
latchSupportZ  = 42-0.2;
latchSupportTranslate = [82,18,0];

module latchSupport(){
  translate(latchSupportTranslate)
  rotate([0,-90,0])
  linear_extrude(latchSupportZ)
    import(dxfFileName, layer = latchSupportLayerName);
}
//latchSupport();

module latchSide(){
  rotate([90,0,0])
    translate([15.0175,18.6])
      linear_extrude(latchSideX)
        import(dxfFileName, layer = latchSideLayerName);
}
//latchSide();

module latchPlate(){
  intersection(){
    //translate([0,-latchInnerY,0])
    cube([latchPlateX,latchInnerY,latchSideX]);
    translate([0,latchSideX+latchInnerY,0])
    rotate([90,0,0])
        translate([15.0175,18.6])
          linear_extrude(latchSideX+latchInnerY)
            import(dxfFileName, layer = latchSideLayerName);
  }
}
//latchPlate();

module latch(){
  latchSide();
  translate([0,latchInnerY+latchSideX,0])
    latchSide();
  latchPlate();
}
//latch();

module trap(){
  epsilon = 0.1;
  linear_extrude(fullZ)
    import(dxfFileName, layer = trapInnerLayerName);
  linear_extrude(fullZ-traplipZ-epsilon)
    import(dxfFileName, layer = trapOuterLayerName);
}
/*
projection(cut=true)
translate([0,0,2])
rotate([90,0,0])
//translate([-70.45,-1,0])
trap();
*/

//RAIL ///////////////////
railBaseZ                = 8; //7.25; // as per dxf and inserts
railBaseYOffset          = 29;
railBaseWidth            = 14.5;
railZ                    = 14;
railWallThickness        = 3;
railLeftBlockSpace       = 5.5;
railLeftBlockThickness   = railBaseWidth - railWallThickness - railLeftBlockSpace;
railRightBlockThickness  = 8;
railHoleStdLength        = 9;
railBackLayerName        = "railBack";
railFrontLayerName       = "railFront";
railBaseLayerName        = "railBase";
railLeftBlockLayerName   = "railLeftBlock";
railRightBlockLayerName  = "railRightBlock";
railHole1LayerName       = "railHole1";
railHole2LayerName       = "railHole2";
railHole3LayerName       = "railHole3";
railHole4LayerName       = "railHole4";
railHole4TopLayerName    = "railHole4_top";

//railWidthFix = 0.5;


module railBase(){
  translate([0,railBaseYOffset,0])
    linear_extrude(railBaseZ)
      import(dxfFileName, layer = railBaseLayerName);
}
//railBase();

module railFront(){
  translate([0,-railBaseWidth+railWallThickness,0])
    rotate([90,0,0])
      linear_extrude(railWallThickness)
        import(dxfFileName, layer = railFrontLayerName);
}
//railFront();

module railBack(){
  //translate([0,-railBaseWidth+railWallThickness,0])
    rotate([90,0,0])
      linear_extrude(railWallThickness)
        import(dxfFileName, layer = railBackLayerName);
}
//railBack();

module railLeftBlock(){
  translate([0,- railWallThickness - railLeftBlockSpace,0])
    rotate([90,0,0])
      linear_extrude(railLeftBlockThickness)
        import(dxfFileName, layer = railLeftBlockLayerName);
}
//railLeftBlock();

module railRightBlock(){
  intersection(){
    translate([0,railBaseYOffset,0])
      linear_extrude(railZ)
        import(dxfFileName, layer = railBaseLayerName);
    
  translate([0,- railWallThickness,0])
    rotate([90,0,0])
      linear_extrude(railRightBlockThickness)
        import(dxfFileName, layer = railRightBlockLayerName);
  }
}
//railRightBlock();

module hole1(){
  translate([0,-railBaseWidth+railWallThickness+railLeftBlockSpace,0])
    rotate([90,0,0])
      linear_extrude(railHoleStdLength)
        import(dxfFileName, layer = railHole1LayerName);
}
//hole1();
module hole2(){
  translate([0,-railBaseWidth+railWallThickness+railLeftBlockSpace,0])
    rotate([90,0,0])
      linear_extrude(railHoleStdLength)
        import(dxfFileName, layer = railHole2LayerName);
}
//hole2();

module hole3(){
  translate([0,-railBaseWidth+railWallThickness+railLeftBlockSpace,0])
    rotate([90,0,0])
      linear_extrude(railHoleStdLength)
        import(dxfFileName, layer = railHole3LayerName);
}
//hole3();
module hole3Experimental(){
translate([0,-railBaseWidth+railWallThickness+railLeftBlockSpace,0])
  translate([260.75,-railHoleStdLength,0])
    rotate([0,0,3])
      translate([-260.75,railHoleStdLength,0])
        rotate([90,0,0])
          linear_extrude(railHoleStdLength)
            import(dxfFileName, layer = railHole3LayerName);
}
//hole3Experimental();

module hole4(top){
  //translate([0,-railBaseWidth+railWallThickness+railLeftBlockSpace,0])
  holeLayerName = top ? railHole4TopLayerName : railHole4LayerName;
    rotate([90,0,0])
      linear_extrude(railHoleStdLength)
        import(dxfFileName, layer = holeLayerName);
}
//hole4();

module topRail(top=true){
  difference(){
    union(){
      railBase();
      railFront();
      railBack();
      railLeftBlock();
      railRightBlock();
    }
    union(){
      hole1();
      hole2();
      hole3();
      hole4(top);
    }
  }
}
//topRail();

module bottomRail(){
  mirror([1,0,0])
   topRail(false);
} 
//bottomRail();


//// SPRING

springThickLayerName = "spring";
springThinLayerName  = "spring thin";
springHoleLayerName  = "spring holes";
springAlphaLayerName = "spring alpha";
springCharlieLayerName = "spring charlie";
springZ              = 14; //15;  update to allow for inserts
springHolesZ         = 9; //10; update to allow for inserts

layerNameVec = [springCharlieLayerName, springAlphaLayerName, springThinLayerName,springThickLayerName];

layerNameIndex = 0;

module springHoles(dir){
  translate([0,0,dir*springHolesZ]) //springZ/2.0])
    rotate([90,0,0])
      linear_extrude(springZ)
        import(dxfFileName, layer = springHoleLayerName);
}
//springHoles();

module springBody(dir){
  layerName = layerNameVec[layerNameIndex];
  Z = dir == -1 ? -springZ : 0;
  translate([0,0,Z])
    linear_extrude(springZ)
        import(dxfFileName, layer = layerName);
}
//springBody();

module bottomSpring(){
  difference(){
    springBody(1);
    springHoles(1);
  }
}
//bottomSpring();

module topSpring(){
  rotate([180,0,0])
    difference(){
      springBody(-1);
      springHoles(-1);
    }
}
//topSpring();

//// Button

buttonLayerName = "handle Button";

module button(){
  rotate([0,180,0])
    rotate_extrude()
      import(dxfFileName, layer = buttonLayerName);
}
//button();

/// // holeTest();

module holeTest(){
  difference(){
    union(){
      cube([15,15,5]);
      translate([10,10,0])
        cylinder(h=10,d=10);
    }
    translate([3,5,0])
      cylinder(h=15,d=3);
    translate([7,0,2.5])
      rotate([-90,0,0])
        cylinder(h=15,d=3);
      
  }
}
//holeTest();

module plateTest(){
  difference(){
    intersection(){
      translate([-10,-10,0])
      cube([20,27.5,20]);
      translate([58.9,-19.75,0])
        plateAll();
    }
    translate([0,12.5,0])
      cylinder(h=3,d=insertDia);
  }
}
plateTest();

//// multi hole test

m3Vec = [3,3.2,3.4];
insVec = [4.2,4.4,4.6];
mX = 29;
mY = 28;
mZ = 13;
insZ = 6;
mBaseZ = 4;

module makeCyls(vec,Z){
  space = 2.5 + max(vec);
  initS = 2;
  nb = len(vec);
  for(i= [0:1:nb-1])
      translate([initS+0.75*max(vec) + (1+space)*(i),0,0])
        cylinder(h=Z,d=vec[i]);
}
//    makeCyls(m3Vec,mX);

module multiHoleTest(){ 
  difference(){
    cube([mX,mY,mZ]);
    union(){
      translate([0,0,mBaseZ])
        rotate([-90,0,0])
          makeCyls(m3Vec,mY);
      translate([0,mBaseZ,mZ-insZ])
        makeCyls(m3Vec,insZ);
      translate([mX-insZ,0,mBaseZ])
        rotate([0,90,0])
          rotate([0,0,90])
            makeCyls(insVec,insZ);
      translate([0,mY-mBaseZ,mZ-insZ])
        makeCyls(insVec,insZ);
    }
  }
}
//multiHoleTest();