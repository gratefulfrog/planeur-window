use <window_maker_04.scad>


module dPlate(){
  rotate([180,0,0])
    plateAll();
}

dPlate();

module dButton(){
  translate([12,-60, 10])
    rotate([180,0,0])
      button();
}
dButton();

module dSprings(){
  translate([0,0,10])
    rotate([90,0,0])
      topSpring();
  translate([0,-120,10])
    rotate([-90,0,0])
      bottomSpring();
}
dSprings();

module dPivotBlock(){
  translate([70,-77,10])
    pivotBlock();
}
dPivotBlock();

module dLatch(){
  translate([56,-37,40])
    rotate([180,0,0])
      latch();
}
dLatch();

module dTrap(){
  translate([0,-117,10])
  trap();
}
dTrap();

module dRails(){
  translate([-185,20,5])
    rotate([90,0,0])
      topRail();
  translate([-185,-135,5])
    rotate([-90,180,0])
      bottomRail();
}
dRails();